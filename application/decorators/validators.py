from rest_framework.response import Response
from rest_framework.request import Request
from erebor.products.serializers import ProductSerializer
from .controllers import UtilsSerializer
from rest_framework import status


def validate_data(main_function) -> Response:
    def initial_body_validation(request):
        if len(request.data) == 0:
            return Response(
                {"There wasn't data entered"}, status=status.HTTP_400_BAD_REQUEST
            )

        if len(request.data.get("products")) == 0:
            return Response(
                {"There weren't products inside the request"},
                status=status.HTTP_400_BAD_REQUEST,
            )

    def inner_validation(request: Request, *args: tuple, **kwargs: dict) -> Response:

        initial_validation_response = initial_body_validation(request)

        if initial_validation_response:
            return initial_validation_response

        serializer_handler = UtilsSerializer(request.data["products"], ProductSerializer)

        if not serializer_handler.is_valid():
            return serializer_handler.error_response

        return main_function(
            request, model_serializer=serializer_handler.model_serializer
        )

    return inner_validation
