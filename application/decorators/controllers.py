from typing import List
from rest_framework.response import Response
from rest_framework import status
from erebor.products.serializers import ProductSerializerToSave


class UtilsSerializer:
    def __init__(self, data: dict or list, serializer_class: object):
        self.data = data
        self.serializer_class = serializer_class
        self.products_report: List = []
        self.model_serializer = None
        self._error_response = None

    def is_valid(self):
        if isinstance(self.data, dict):
            self._validate_object()

        if isinstance(self.data, list):
            self._validate_object_list()

        if self._error_response:
            return False

        if isinstance(self.data, dict):
            self.model_serializer = ProductSerializerToSave(data=self.data)
            self.model_serializer.is_valid()

        if isinstance(self.data, list):
            self.model_serializer = ProductSerializerToSave(data=self.data, many=True)
            self.model_serializer.is_valid()

        return True

    @property
    def error_response(self):
        return self._error_response

    def _validate_object(self):
        self._validate_data(self.data)
        self._update_error_response()

    def _validate_object_list(self):
        for product_data in self.data:
            self._validate_data(product_data)

        self._update_error_response()

    def _validate_data(self, object_data):
        product_serializer = self.serializer_class(data=object_data)

        if not product_serializer.is_valid():
            string_set = self._obtain_errors_list(product_serializer)
            self.products_report.append(
                {"product_id": object_data.get("id"), "errors": string_set}
            )

    def _update_error_response(self):
        if len(self.products_report) > 0:
            self._error_response = self._generate_error_response(self.products_report)

    def _obtain_errors_list(self, serializer):
        return [serializer.errors[key][0] for key in serializer.errors.keys()]

    def _generate_error_response(self, products_report):
        return Response(
            {
                "status": "ERROR",
                "products_report": products_report,
                "number_of_products_unable_to_parse": len(products_report),
            },
            status=status.HTTP_422_UNPROCESSABLE_ENTITY,
        )
