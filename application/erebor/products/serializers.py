from rest_framework import serializers
from .models import Product
from django.db.models import TextField, FloatField, IntegerField


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"

    def validate_value(self, data):
        if not (0 < data < 99999.9):
            raise serializers.ValidationError("Invalid value")
        return data

    def validate_discount_value(self, data):
        if data > self.initial_data["value"]:
            raise serializers.ValidationError("Invalid discount value")
        return data

    def validate_stock(self, data):
        if data <= -1:
            raise serializers.ValidationError("Invalid stock value")
        return data


class ProductSerializerToSave(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = "__all__"
