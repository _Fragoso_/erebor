from rest_framework.decorators import api_view
from rest_framework.request import Request
from decorators.validators import validate_data
from .controllers import save_products, obtain_products
from rest_framework.response import Response


@api_view(["POST"])
@validate_data
def bulk_insert(request: Request, *args: tuple, **kwargs: dict) -> Response:
    return save_products(request, *args, **kwargs)


@api_view(["GET"])
def list_products(request: Request, *args: tuple, **kwargs: dict) -> Response:
    return obtain_products(request, *args, **kwargs)
