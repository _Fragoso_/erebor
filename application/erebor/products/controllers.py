from rest_framework.response import Response
from rest_framework import status
from .models import Product
from .serializers import ProductSerializer


def save_products(request, *args, **kwargs):
    kwargs["model_serializer"].save()
    return Response({"status": "OK"}, status=status.HTTP_200_OK)


def obtain_products(request, *args, **kwargs):
    products_obtained = Product.objects.all()

    if len(products_obtained) == 0:
        return Response(
            {"There aren't products inside the database"},
            status=status.HTTP_400_BAD_REQUEST,
        )

    products_serializer = ProductSerializer(products_obtained, many=True)

    return Response({"products": products_serializer.data}, status=status.HTTP_302_FOUND)
