from django.urls import path
from .views import bulk_insert, list_products

urlpatterns = [path(r"bulk_insert", bulk_insert), path(r"", list_products)]
