from django.db import models
from django.core.validators import (
    MinLengthValidator,
    MaxLengthValidator,
    MinValueValidator,
    MaxValueValidator,
)
from django.core.exceptions import ValidationError


class Product(models.Model):
    id = models.TextField(primary_key=True, max_length=255)
    name = models.TextField(
        validators=[
            MinLengthValidator(3, message="Invalid product name"),
            MaxLengthValidator(55, message="Invalid product name"),
        ]
    )
    value = models.FloatField()
    discount_value = models.FloatField()
    stock = models.IntegerField()

    class Meta:
        db_table = "products"
